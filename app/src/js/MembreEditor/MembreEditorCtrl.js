(function () {
	'use strict';

	function MembreEditorCtrl(MembresService, $stateParams, $location, $state) {
		var vm = this;

		vm.settings = {
			title : 'Nouveau Membre'
		};

		vm.state = {
			sidebar : false,
			spinner : false,
		};

		vm.backAction = function() {
			if ($state.current.name === 'MembreEdit') {
				$state.go('MembreDetail', { id : $stateParams.id });
			} else {
				$state.go('Membres');
			}
			
		};

		vm.submitMembre = function() {
			if (!(vm.membreModel.nom && vm.membreModel.prenom && vm.membreModel.login && vm.membreModel.password )) {
				return;
			}; 

			if ($state.current.name === 'MembreEdit') {
				return vm.updateMembre();
			} else {
				return vm.createMembre();
			};
		};


		vm.createMembre = function() {
			var membreModel = vm.membreModel || {};
			vm.state.spinner = true;

			MembresService
				.createMembre(membreModel)
				.then(function(membre) {

					$state.go('MembreDetail', { id : membre.data.id });
					vm.state.spinner = false;
				})
				.catch(function(e) {
					console.log(e);
				});
		};

		vm.updateMembre = function() {
			var membre = {
				nom : vm.membreModel.nom,
				prenom : vm.membreModel.prenom,
				login : vm.membreModel.login,
				password : vm.membreModel.password,
				id : $stateParams.id
			};

			var membreModel = membre || {};
			vm.state.spinner = true;

			MembresService
				.updateMembre(membre.id, membreModel)
				.then(function() {
					$state.go('MembreDetail', { id : membre.id });
					vm.state.spinner = false;
				})
				.catch(function(e) {
					console.log(e);
				});
		};

		vm.getMembre = function(id) {
			vm.state.spinner = true;

			MembresService
				.getMembreById(id)
				.then(function() {
					vm.membreModel = MembresService.data;
					vm.state.spinner = false;
				})
				.catch(function(e) {
					console.log(e)
				});
		};
		
		function init (argument) {
			if ($state.current.name === 'MembreEdit') {
				vm.getMembre($stateParams.id);
				vm.settings.title = 'Editer un membre'
			}
		}

		init();
	}


	MembreEditorCtrl.$inject = ['MembresService', '$stateParams', '$location', '$state'];


	angular
	    .module('bibliobrowser')
	    .controller('MembreEditorCtrl', MembreEditorCtrl);

})();
		