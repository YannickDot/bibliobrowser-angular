(function () {
	'use strict';

	function LivreDetailCtrl(LivresService, $stateParams, $state) {
		var vm = this;

		vm.settings = {
			title : 'Livre'
		};

		vm.state = {
			sidebar : false,
			spinner : true,
		};

		vm.backAction = function() {
			$state.go('Livres');
		};

		vm.getLivre = function(isbn) {
			vm.state.spinner = true;

			LivresService
				.getLivreByISBN(isbn)
				.then(function() {
					vm.livre = LivresService.data;
					vm.state.spinner = false;
				})
				.catch(function() {

				});
		};

		vm.deleteLivre = function() {
			var isbn = $stateParams.isbn;
			vm.state.spinner = true;

			LivresService
				.deleteLivre(isbn)
				.then(function() {
					vm.state.spinner = false;
					$state.go('Livres');
				})
				.catch(function() {

				});
		};

		vm.getLivre($stateParams.isbn);
	}



	LivreDetailCtrl.$inject = ['LivresService', '$stateParams', '$state'];


	angular
	    .module('bibliobrowser')
	    .controller('LivreDetailCtrl', LivreDetailCtrl);

})();
		