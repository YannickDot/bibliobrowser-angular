(function () {
	'use strict';

	function HomeCtrl(AuthService, LivresService, MembresService, EmpruntsService, localStorageService) {
		var vm = this;

		vm.settings = {
			title : 'BiblioBrowser'
		};
		
		vm.state = {
			sidebar : false,
			spinner : false,
			rootApiCard : true,
			authCard : AuthService.isAuth()
		};

		//vm.rootApi = localStorageService.get('ROOT_API', '');

		vm.rootApi = 'http://localhost:8080/biblio/resources';

		// vm.toggleSidebar = function() {
		// 	vm.state.sidebar = !vm.state.sidebar;
		// };

		vm.setRootApi = function() {

			localStorageService.set('ROOT_API', vm.rootApi);
			LivresService.setRootApi(vm.rootApi);
			MembresService.setRootApi(vm.rootApi);
			EmpruntsService.setRootApi(vm.rootApi);
			//vm.state.rootApiCard = false;
		};

		vm.setAuthorization = function() {
			AuthService.setAuthorization(vm.auth.username, vm.auth.password);
			vm.state.authCard = false;
		};

		vm.setRootApi();


	}


	HomeCtrl.$inject = ['AuthService', 'LivresService', 'MembresService', 'EmpruntsService', 'localStorageService'];


	angular
	    .module('bibliobrowser')
	    .controller('HomeCtrl', HomeCtrl);

})();
		