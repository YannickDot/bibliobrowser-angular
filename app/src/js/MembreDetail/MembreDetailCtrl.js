(function () {
	'use strict';

	function MembreDetailCtrl(MembresService, $stateParams, $state) {
		var vm = this;

		vm.settings = {
			title : 'Membre'
		};

		vm.state = {
			sidebar : false,
			spinner : false,
		};

		vm.backAction = function() {
			$state.go('Membres');
		};

		vm.getMembre = function(id) {
			vm.state.spinner = true;

			MembresService
				.getMembreById(id)
				.then(function() {
					vm.membre = MembresService.data;
					vm.state.spinner = false;
				})
				.catch(function() {

				});
		};

		vm.deleteLivre = function() {
			var id = $stateParams.id;
			vm.state.spinner = true;

			MembresService
				.deleteMembre(id)
				.then(function() {
					vm.state.spinner = false;
					$state.go('Membres');
				})
				.catch(function() {

				});
		};

		vm.getMembre($stateParams.id);
	}


	MembreDetailCtrl.$inject = ['MembresService', '$stateParams', '$state'];


	angular
	    .module('bibliobrowser')
	    .controller('MembreDetailCtrl', MembreDetailCtrl);

})();
		