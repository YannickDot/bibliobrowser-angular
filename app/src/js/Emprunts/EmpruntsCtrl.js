(function () {
	'use strict';

	function EmpruntsCtrl(EmpruntsService) {
		var vm = this;

		vm.settings = {
			title : 'Emprunts'
		};
		
		vm.state = {
			sidebar : false,
			spinner : true,
		};


		vm.getEmprunts = function() {
			vm.state.spinner = true;

			EmpruntsService
				.getEmprunts()
				.then(function() {
					vm.emprunts = EmpruntsService.data;
					vm.state.spinner = false;
				});
		};

		vm.getEmprunts();
	}


	EmpruntsCtrl.$inject = ['EmpruntsService'];


	angular
	    .module('bibliobrowser')
	    .controller('EmpruntsCtrl', EmpruntsCtrl);

})();

