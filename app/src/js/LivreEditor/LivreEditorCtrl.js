(function () {
	'use strict';

	function LivreEditorCtrl(LivresService, $stateParams, $location, $state) {
		var vm = this;

		vm.settings = {
			title : 'Nouveau Livre'
		};

		vm.state = {
			sidebar : false,
			spinner : false,
		};

		vm.backAction = function() {
			if ($state.current.name === 'LivreEdit') {
				$state.go('LivreDetail', { isbn : $stateParams.isbn });
			} else {
				$state.go('Livres');
			}
			
		};

		vm.submitLivre = function() {
			if (!(vm.livreModel.titre && vm.livreModel.auteur)) {
				return;
			}; 

			if ($state.current.name === 'LivreCreate') {
				return vm.createLivre();
			} else {
				return vm.updateLivre();
			};
		};


		vm.createLivre = function() {
			var livreModel = vm.livreModel || {};
			vm.state.spinner = true;

			LivresService
				.createLivre(livreModel)
				.then(function(livre) {

					$state.go('LivreDetail', { isbn : livre.data.ISBN });
					vm.state.spinner = false;
				})
				.catch(function(e) {
					console.log(e);
				});
		};

		vm.updateLivre = function() {
			var livre = {
				titre : vm.livreModel.titre,
				auteur : vm.livreModel.auteur,
				ISBN : $stateParams.isbn
			};

			var livreModel = livre || {};
			vm.state.spinner = true;

			LivresService
				.updateLivre(livre.ISBN, livreModel)
				.then(function() {
					$state.go('LivreDetail', { isbn : livre.ISBN });
					vm.state.spinner = false;
				})
				.catch(function(e) {
					console.log(e);
				});
		};

		vm.getLivre = function(isbn) {
			vm.state.spinner = true;

			LivresService
				.getLivreByISBN(isbn)
				.then(function() {
					vm.livreModel = LivresService.data;
					vm.state.spinner = false;
				})
				.catch(function(e) {
					console.log(e)
				});
		};
		
		function init (argument) {
			if ($state.current.name === 'LivreEdit') {
				vm.getLivre($stateParams.isbn);
				vm.settings.title = 'Editer un livre'
			}
		}

		init();

		

	}


	LivreEditorCtrl.$inject = ['LivresService', '$stateParams', '$location', '$state'];


	angular
	    .module('bibliobrowser')
	    .controller('LivreEditorCtrl', LivreEditorCtrl);

})();
		