(function () {
  'use strict';

  function SearchDirective() {
      return {
        templateUrl: 'js/Shared/UI/Search/search.html' ,
        restrict: 'E',
        scope: {
        	visible : '='
        },
        replace: false,
        transclude: true,
        link: function postLink(scope, element, attrs) {}
      };
    }

  angular
    .module('bibliobrowser')
    .directive('search', SearchDirective);

})();
		