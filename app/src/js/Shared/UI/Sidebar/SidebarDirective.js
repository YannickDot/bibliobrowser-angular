(function () {
  'use strict';

  function SidebarDirective() {
      return {
        templateUrl: 'js/Shared/UI/Sidebar/sidebar.html',
        restrict: 'E',
        scope: {
        	open : '=',
        },
        replace: true,
        transclude: true,
        link: function postLink(scope, element, attrs) {
        	scope.toggleSidebar = function() {
        	  scope.open = !scope.open;
        	};
        }
      };
    }

  angular
    .module('bibliobrowser')
    .directive('sidebar', SidebarDirective);

})();
		