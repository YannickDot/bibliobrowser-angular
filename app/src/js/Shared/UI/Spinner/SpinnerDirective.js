(function () {
  'use strict';

  function SpinnerDirective() {
      return {
        templateUrl: 'js/Shared/UI/Spinner/spinner.html' ,
        restrict: 'E',
        scope: {
        	spinner : '=',
        },
        replace: true,
        transclude: true,
        link: function postLink(scope, element, attrs) {

        }
      };
    }

  angular
    .module('bibliobrowser')
    .directive('spinner', SpinnerDirective);

})();
		