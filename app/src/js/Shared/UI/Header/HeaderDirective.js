(function () {
  'use strict';

  function HeaderDirective($window) {
      return {
        templateUrl: 'js/Shared/UI/Header/header.html' ,
        restrict: 'E',
        scope: {
        	sidebar : '=',
          title : '@',
          backbtn : '@',
          backaction : '&',
          searchbtn : '@',
          searchstate : '='
        },
        replace: false,
        transclude: true,
        link: function postLink(scope, element, attrs) {
        	scope.search = function() {
        	  scope.searchstate = !scope.searchstate;
        	};

          scope.action = function() {
            if(scope.backbtn) {
              //$window.history.back();
              scope.backaction();
            } else {
              scope.sidebar = !scope.sidebar;
            };
          };

        }
      };
    }

  HeaderDirective.$inject = ['$window'];

  angular
    .module('bibliobrowser')
    .directive('header', HeaderDirective);

})();
		