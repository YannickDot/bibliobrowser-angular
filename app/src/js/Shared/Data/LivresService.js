(function () {
	'use strict';

	function LivresService($http) {
		var BASE_URL = '';
		var Authorization = '';

		var URL_LIVRES = '/livres';
		var queryParam = '?';
		var queryParamSeparator = '&';
		var queryParamTitle = 't=';
		var queryParamAuthor = 'a=';
		
	  var LivresService = {};

	  function setRootApi (rootApi){
	  	BASE_URL = rootApi;
	  };

	  function setAuth (authData) {
	  	Authorization = authData;
	  }

	  function getData (url) {
	  	var request = {
	  		method: 'GET',
	  		 url: url,
	  		 headers: {
	  		   'Authorization' : Authorization,
					 'Accept': 'application/json, text/plain, * / *'
	  		 },
	  	};
	  	console.log(request);

	  	return $http(request)
	  	  .success(function (data) {
	  	    LivresService.data = data;

	  	  })
	  	  .error(function (data) {
	  	    
	  	  });
	  };


	  function getLivres () {
	  	var url = BASE_URL + URL_LIVRES;

	  	return getData(url);
	  };


	  function getLivreByISBN (isbn) {
	  	var isbn = isbn || 0;
	  	var url = BASE_URL + URL_LIVRES + '/' + isbn;

	  	return getData(url);
	  };


	  function getLivresFiltered (title, authorName) {
	  	var URLtitle = '';
	  	var URLauthorName = '';
	  	var url = BASE_URL + URL_LIVRES + queryParam;

	  	if (title) {
	  		URLtitle = queryParamTitle + title;
	  	};

	  	if (authorName) {
	  		URLauthorName = queryParamAuthor + authorName;
	  	};
	  	
	  	if (title && authorName) {
	  		url = url + URLtitle + queryParamSeparator + URLauthorName; 
	  	} else{
	  		url = url + URLtitle + URLauthorName; 
	  	};

	  	console.log(url);

	  	return getData(url);
	  };

	  function createLivre (livreModel) {
	  	var url = BASE_URL + URL_LIVRES;

	  	var request = {
	  		method: 'POST',
	  		 url: url,
	  		 headers: {
	  		   'Authorization' : Authorization,
					 'Accept': 'application/json, text/plain, * / *'
	  		 },
	  		 data: livreModel,
	  	};

	  	return $http(request)
	  	  .success(function (data) {
	  	    console.log('Livre ' + livreModel.titre + ' > created');
	  	  })
	  	  .error(function (data) {
	  	    
	  	  });
	  }

	  function updateLivre (isbn, livreModel) {
	  	var url = BASE_URL + URL_LIVRES + '/' + isbn;

	  	var request = {
	  		method: 'PUT',
	  		 url: url,
	  		 headers: {
	  		   'Authorization' : Authorization,
					 'Accept': 'application/json, text/plain, * / *'
	  		 },
	  		 data: livreModel,
	  	};

	  	return $http(request)
	  	  .success(function (data) {
	  	    console.log('Livre ' + livreModel.ISBN + ' > updated');
	  	  })
	  	  .error(function (data) {
	  	    
	  	  });
	  }


	  function deleteLivre (isbn) {
	  	var isbn = isbn || 0;
	  	var url = BASE_URL + URL_LIVRES + '/' + isbn;

	  	var request = {
	  		method: 'DELETE',
	  		 url: url,
	  		 headers: {
	  		   'Authorization' : Authorization,
					 'Accept': 'application/json, text/plain, * / *'
	  		 },
	  	};

	  	return $http(request)
	  	  .success(function (data) {
	  	    console.log('Livre ISBN ' +isbn+ ' > deleted');
	  	  })
	  	  .error(function (data) {
	  	    
	  	  });
	  }


	  LivresService.setRootApi = setRootApi;
	  LivresService.getLivres = getLivres;
	  LivresService.getLivreByISBN = getLivreByISBN;
	  LivresService.getLivresFiltered = getLivresFiltered;
	  LivresService.deleteLivre = deleteLivre;
	  LivresService.createLivre = createLivre;
	  LivresService.updateLivre = updateLivre;
	  LivresService.setAuth = setAuth;

	  return LivresService;

	}


	LivresService.$inject = ['$http'];

	angular
	  .module('bibliobrowser')
	  .factory('LivresService', LivresService);

})();
		