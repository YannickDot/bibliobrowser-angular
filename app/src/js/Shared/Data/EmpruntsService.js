(function () {
	'use strict';

	function EmpruntsService($http) {
		var BASE_URL = '';
		var Authorization = '';

		var URL_EMPRUNTS = '/emprunts';
		var queryParam = '?';
		var queryParamSeparator = '&';
		var queryParamTitle = 't=';
		var queryParamAuthor = 't=';

	  var EmpruntsService = {};

	  function setRootApi (rootApi){
	  	BASE_URL = rootApi;
	  };

	  function setAuth (authData) {
	  	Authorization = authData;
	  }

    function getData (url) {
    	var request = {
    		method: 'GET',
    		 url: url,
    		 headers: {
    		   'Authorization' : Authorization,
  				 'Accept': 'application/json, text/plain, * / *'
    		 },
    	};
    	console.log(request);

    	return $http(request)
    	  .success(function (data) {
    	    EmpruntsService.data = data;

    	  })
    	  .error(function (data) {
    	    
    	  });
    };


	  function getEmprunts () {
	  	var url = BASE_URL + URL_EMPRUNTS;

	  	return getData(url);
	  };


	  EmpruntsService.setRootApi = setRootApi;
	  EmpruntsService.getEmprunts = getEmprunts;
	  EmpruntsService.setAuth = setAuth;
	  
	  return EmpruntsService;

	}

	EmpruntsService.$inject = ['$http'];

	angular
	  .module('bibliobrowser')
	  .factory('EmpruntsService', EmpruntsService);

})();
		