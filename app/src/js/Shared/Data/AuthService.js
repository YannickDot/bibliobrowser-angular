(function () {
	'use strict';

	function AuthService(Base64, LivresService, EmpruntsService, MembresService) {
		var Authorization = '';
	  var AuthService = {};
	  var _isAuthenticated = false;

	  function setAuthorization (username, password){
	  	var authdata = Base64.encode(username + ':' + password);
	  	Authorization = 'Basic ' + authdata;
	  	_isAuthenticated = true;

	  	notifyAuth();
	  };

	  function notifyAuth () {
	  	LivresService.setAuth(Authorization); 
	  	EmpruntsService.setAuth(Authorization);
	  	MembresService.setAuth(Authorization);
	  }

	  function getAuth () {
	  	return Authorization;
	  }

	  function isAuth () {
	  	return _isAuthenticated;
	  }

	  AuthService.getAuth = getAuth;
	  AuthService.setAuthorization = setAuthorization;
	  AuthService.isAuth = isAuth;


	  return AuthService;

	}

	AuthService.$inject = ['Base64', 'LivresService', 'EmpruntsService', 'MembresService'];

	angular
	  .module('bibliobrowser')
	  .factory('AuthService', AuthService);

})();
		