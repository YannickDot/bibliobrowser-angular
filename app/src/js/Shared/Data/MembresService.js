(function () {
	'use strict';

	function MembresService($http) {
		var BASE_URL = '';
		var Authorization = '';

		var URL_MEMBRES = '/membres';
		var queryParam = '?';
		var queryParamSeparator = '&';
		var queryParamNom = 'n=';
		var queryParamPrenom = 'p=';	

	  var MembresService = {};

	  function setRootApi (rootApi){
	  	BASE_URL = rootApi;
	  };

	  function setAuth (authData) {
	  	Authorization = authData;
	  }


    function getData (url) {
    	var request = {
    		method: 'GET',
    		 url: url,
    		 headers: {
    		   'Authorization' : Authorization,
  				 'Accept': 'application/json, text/plain, * / *'
    		 },
    	};
    	console.log(request);

    	return $http(request)
    	  .success(function (data) {
    	    MembresService.data = data;

    	  })
    	  .error(function (data) {
    	    
    	  });
    };


	  function getMembres () {
	  	var url = BASE_URL + URL_MEMBRES;

	  	return getData(url);
	  };

	  function getMembreById (id) {
	  	var id = id || 0;
	  	var url = BASE_URL + URL_MEMBRES + '/' + id;

	  	return getData(url);
	  };

	  function getMembresFiltered (nom, prenom) {
	  	var URLnom = '';
	  	var URLprenom = '';
	  	var url = BASE_URL + URL_MEMBRES + queryParam;

	  	if (nom) {
	  		URLnom = queryParamNom + nom;
	  	};

	  	if (prenom) {
	  		URLprenom = queryParamPrenom + prenom;
	  	};
	  	
	  	if (nom && prenom) {
	  		url = url + URLnom + queryParamSeparator + URLprenom; 
	  	} else{
	  		url = url + URLnom + URLprenom; 
	  	};

	  	console.log(url);

	  	return getData(url);
	  };


    function createMembre (membreModel) {
    	var url = BASE_URL + URL_MEMBRES;

    	var request = {
    		method: 'POST',
    		 url: url,
    		 headers: {
    		   'Authorization' : Authorization,
  				 'Accept': 'application/json, text/plain, * / *'
    		 },
    		 data: membreModel,
    	};

    	return $http(request)
    	  .success(function (data) {
    	    console.log('Membre ' + membreModel.prenom +' '+ membreModel.nom + ' > created');
    	  })
    	  .error(function (data) {
    	    
    	  });
    }

    function updateMembre (id, membreModel) {
    	var url = BASE_URL + URL_MEMBRES + '/' + id;

    	var request = {
    		method: 'PUT',
    		 url: url,
    		 headers: {
    		   'Authorization' : Authorization,
  				 'Accept': 'application/json, text/plain, * / *'
    		 },
    		 data: membreModel,
    	};

    	return $http(request)
    	  .success(function (data) {
    	    console.log('Membre ' + membreModel.id + ' > updated');
    	  })
    	  .error(function (data) {
    	    
    	  });
    }

    function deleteMembre (id) {
    	var id = id || 0;
    	var url = BASE_URL + URL_MEMBRES + '/' + id;

    	var request = {
    		method: 'DELETE',
    		 url: url,
    		 headers: {
    		   'Authorization' : Authorization,
  				 'Accept': 'application/json, text/plain, * / *'
    		 },
    	};

    	return $http(request)
    	  .success(function (data) {
    	    console.log('Membre ' +id+ ' > deleted');
    	  })
    	  .error(function (data) {
    	    
    	  });
    }



	  MembresService.setRootApi = setRootApi;
	  MembresService.setAuth = setAuth;
	  MembresService.getMembres = getMembres;
	  MembresService.getMembreById = getMembreById;
	  MembresService.getMembresFiltered = getMembresFiltered;
	  MembresService.createMembre = createMembre;
	  MembresService.updateMembre = updateMembre;
	  MembresService.deleteMembre = deleteMembre;
	 

	  return MembresService;

	}

	MembresService.$inject = ['$http'];

	angular
	  .module('bibliobrowser')
	  .factory('MembresService', MembresService);

})();
		