(function () {
	'use strict';

	function LivresCtrl(LivresService) {
		var vm = this;

		vm.settings = {
			title : 'Livres'
		};
		
		vm.state = {
			sidebar : false,
			spinner : true,
			search : false,
		};

		vm.toggleSearch = function() {
			vm.state.search = !vm.state.search;
		};

		vm.clearSearch = function() {
			vm.search.titre = '';
			vm.search.auteur = '';
			vm.getLivres();
		};

		vm.getLivres = function() {
			vm.state.spinner = true;

			LivresService
				.getLivres()
				.then(function() {
					vm.livres = LivresService.data;
					vm.state.spinner = false;
				});
		};

		vm.searchLivres = function() {
			if (!(vm.search.titre || vm.search.auteur)) {
				return vm.getLivres();
			}; 

			vm.state.spinner = true;

			LivresService
				.getLivresFiltered(vm.search.titre, vm.search.auteur)
				.then(function() {
					vm.livres = LivresService.data;
					vm.state.spinner = false;
				});
		};

		vm.getLivres();

	}


	LivresCtrl.$inject = ['LivresService'];


	angular
	    .module('bibliobrowser')
	    .controller('LivresCtrl', LivresCtrl);

})();
		