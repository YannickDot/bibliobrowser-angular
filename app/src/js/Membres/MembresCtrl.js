(function () {
	'use strict';

	function MembresCtrl(MembresService) {
		var vm = this;

		vm.settings = {
			title : 'Membres'
		};

		vm.state = {
			sidebar : false,
			spinner : true,
			search : false,
		};

		vm.toggleSearch = function() {
			vm.state.search = !vm.state.search;
		};

		vm.clearSearch = function() {
			vm.search.nom = '';
			vm.search.prenom = '';
			vm.getMembres();
		};

		vm.getMembres = function() {
			vm.state.spinner = true;

			MembresService
				.getMembres()
				.then(function() {
					vm.membres = MembresService.data;
					vm.state.spinner = false;
				});
		};

		vm.searchMembres = function() {
			if (!(vm.search.nom || vm.search.prenom)) {
				return vm.getMembres();
			}; 

			vm.state.spinner = true;

			MembresService
				.getMembresFiltered(vm.search.nom, vm.search.prenom)
				.then(function() {
					vm.membres = MembresService.data;
					vm.state.spinner = false;
				});
		};

		vm.getMembres();
	}


	MembresCtrl.$inject = ['MembresService'];


	angular
	    .module('bibliobrowser')
	    .controller('MembresCtrl', MembresCtrl);

})();
		