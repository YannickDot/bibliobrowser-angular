var gulp        = require('gulp');
var concat 		= require('gulp-concat');
var browserSync = require('browser-sync');
var uglify      = require('gulp-uglify');
var minifyCSS   = require('gulp-minify-css');

var paths = {
	in : {	
		browserify : './app/src/js/app.js',
		js : ['app/src/js/**/*.js', '!app/src/js/*.js', '!app/src/js/lib/*.js'],
        jsNg : ['app/src/js/*.js', 'app/src/js/lib/*.js'],
		css : 'app/src/css/**/*.css',
		html : 'app/src/*.html',
		templates : 'app/src/js/**/*.html' 
	},
	out : {
		js : 'app/dist/js/',
		css: 'app/dist/css',
		html : 'app/dist/',
		templates : 'app/dist/js/' 
	},
	serverDir : './app/dist/'
};

gulp.task('js', function () {

  return gulp.src(paths.in.js)
    //.pipe(uglify())
    .pipe(concat('bundle.min.js'))
    .pipe(gulp.dest(paths.out.js))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('js-angular', function () {

  return gulp.src(paths.in.jsNg)
    .pipe(gulp.dest(paths.out.js))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('templates', function () {

  return gulp.src(paths.in.templates)
    .pipe(gulp.dest(paths.out.templates))
    .pipe(browserSync.reload({stream:true}));
});


gulp.task('css', function() {
  gulp.src(paths.in.css)
    .pipe(minifyCSS())
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(paths.out.css))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('html', function() {
  gulp.src(paths.in.html)
    .pipe(gulp.dest(paths.out.html))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: paths.serverDir,
        },
        port: 9999,
        browser: "google chrome canary",
        open: true,
        injectChanges: true,
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});


gulp.task('serve', ['js', 'js-angular', 'templates', 'html', 'css', 'browser-sync'], function () {
    gulp.watch(paths.in.js, ['js']);
    gulp.watch(paths.in.jsNg, ['js-angular']);
    gulp.watch(paths.in.templates, ['templates']);
    gulp.watch(paths.in.html, ['html', 'bs-reload']);
    gulp.watch(paths.in.css, ['css']);
});

